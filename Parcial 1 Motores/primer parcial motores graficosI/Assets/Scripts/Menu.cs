using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
 

    public void Empezar()
    {
        SceneManager.LoadScene("Main Scene");
    }

    public void Salir()
    {
        Application.Quit();
    }

    void Update()
    {
        
    }
}
