using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerController : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    public float fuerzaDeSalto = 10f;
    Rigidbody Rb; 

   
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;  //Este desactiva el cursor del mouse
        Rb = GetComponent<Rigidbody>(); //Esto le dice al juego que RB referencia al Rigidbody del jugador
    }

    // Update is called once per frame
    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown(KeyCode.R)) //Esto hace que el juego se reinicie
        {
            SceneManager.LoadScene("Main Menu");
        }


        if (Input.GetKeyDown("escape")) //Este hace que el cursor del mouse se vuelva a activar

        Cursor.lockState = CursorLockMode.None;

        if(Input.GetKeyDown(KeyCode.Space))
        {
            Rb.AddForce(Vector3.up * fuerzaDeSalto, ForceMode.Impulse);
        }
    }
}
