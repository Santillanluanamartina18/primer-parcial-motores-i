using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class scriptTimer : MonoBehaviour
{
    public Text contador;
    private float tiempo = 60f;
    void Start()
    {
        contador.text = " " + tiempo; 
    }

    // Update is called once per frame
    void Update()
    {
        tiempo--;
        contador.text = " " + tiempo;
    }
}
