using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Velocidad : MonoBehaviour
{
    public float velocidad = 5f;

    void Update()
    {
        // Obtener la entrada de movimiento del jugador (horizontal y vertical)
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");

        // Crear un vector de movimiento usando la entrada y la velocidad
        Vector3 movimiento = new Vector3(movimientoHorizontal, 0f, movimientoVertical) * velocidad * Time.deltaTime;

        // Mover el personaje
        transform.Translate(movimiento);
    }
}
