using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoCircular5 : MonoBehaviour
{
    public class EnemigoCircular : MonoBehaviour
    {
        public float velocidadRotacion = 2f;
        public float radio = 5f;

        private float angulo = 0f;

        void Update()
        {
            angulo += velocidadRotacion * Time.deltaTime;
            float x = Mathf.Cos(angulo) * radio;
            float y = Mathf.Sin(angulo) * radio;

            transform.position = new Vector3(x, y, 0f);

        }
    }
}

