using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo1 : MonoBehaviour
{
    public float velocidad = 5f;
    public float distanciaSeguimiento = 10f;

    private Transform jugador;

    void Start()
    {
        // Encuentra el jugador al inicio
        jugador = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        if (jugador == null)
            return; // Si el jugador no est� presente, no hacer nada

        // Calcula la distancia entre el enemigo y el jugador
        float distanciaAlJugador = Vector3.Distance(transform.position, jugador.position);

        // Si la distancia es menor que la distancia de seguimiento, sigue al jugador
        if (distanciaAlJugador < distanciaSeguimiento)
        {
            // Direcci�n hacia el jugador
            Vector3 direccionAlJugador = (jugador.position - transform.position).normalized;

            // Mueve al enemigo hacia el jugador
            transform.Translate(direccionAlJugador * velocidad * Time.deltaTime);
        }
    }
}
