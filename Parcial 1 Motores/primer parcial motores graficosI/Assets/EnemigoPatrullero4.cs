using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoPatrullero4 : MonoBehaviour
{
    public Transform puntoA;
    public Transform puntoB;
    public float velocidad = 2f;

    void Update()
    {
        transform.Translate(Vector3.right * velocidad * Time.deltaTime);

        if (transform.position.x >= puntoB.position.x)
        {
            // Cambiar direcci�n al llegar al punto B
            transform.localScale = new Vector3(-1f, 1f, 1f);
        }

        if (transform.position.x <= puntoA.position.x)
        {
            // Cambiar direcci�n al llegar al punto A
            transform.localScale = new Vector3(1f, 1f, 1f);
        }
    }
}
