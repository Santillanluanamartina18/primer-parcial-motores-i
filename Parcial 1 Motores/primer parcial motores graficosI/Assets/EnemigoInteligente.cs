using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoInteligente : MonoBehaviour
{
    public float velocidad = 5f;
    public float distanciaSeguimiento = 10f;
    public float distanciaDeteccionObstaculo = 2f;
    public LayerMask capaObstaculos;

    private Transform jugador;

    void Start()
    {
        jugador = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        if (jugador == null)
            return;

        // Calcula la distancia al jugador
        float distanciaAlJugador = Vector3.Distance(transform.position, jugador.position);

        // Verifica si el jugador est� dentro del rango de seguimiento
        if (distanciaAlJugador < distanciaSeguimiento)
        {
            // Verifica si hay obst�culos en el camino
            RaycastHit hit;
            if (Physics.Raycast(transform.position, jugador.position - transform.position, out hit, distanciaDeteccionObstaculo, capaObstaculos))
            {
                // Hay un obst�culo, cambia el comportamiento
                Debug.DrawLine(transform.position, hit.point, Color.red);
                CambiarComportamientoObstaculo();
            }
            else
            {
                // No hay obst�culos, sigue al jugador
                Debug.DrawLine(transform.position, jugador.position, Color.green);
                SeguirJugador();
            }
        }
    }

    void SeguirJugador()
    {
        Vector3 direccionAlJugador = (jugador.position - transform.position).normalized;
        transform.Translate(direccionAlJugador * velocidad * Time.deltaTime);
    }

    void CambiarComportamientoObstaculo()
    {
        // Puedes implementar aqu� cualquier l�gica espec�fica cuando hay un obst�culo
        // Por ejemplo, cambiar la direcci�n, activar alguna habilidad, etc.
        Debug.Log("Obst�culo detectado, cambiando comportamiento");
    }
}
