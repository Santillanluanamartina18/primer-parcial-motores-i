using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class SaltoDoble : MonoBehaviour
{
   public Rigidbody rb;
    public float SaltoVel;
    private bool enElSuelo = true;
    public int maxSaltos = 4;
    public int saltoActual = 0;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

    }
    private void Update()
    {
        if (Input.GetButtonDown("Jump") && (enElSuelo || maxSaltos > saltoActual))

            rb.velocity = new Vector3(0f, SaltoVel, 0f * Time.deltaTime);
        // rb.Addforce(Vector3.up * saltoVel, ForceMode.Impulse); 
        enElSuelo = false;
        saltoActual++;
    }
    private void OnCollisionEnter(Collision colilision)   
    {
        enElSuelo = true;
        saltoActual = 0;
    }


}